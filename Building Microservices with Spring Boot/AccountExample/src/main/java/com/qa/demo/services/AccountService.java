package com.qa.demo.services;


import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import  com.qa.demo.persistence.domain.Account;

@Service
public class AccountService {

    private List<Account> account= new ArrayList<>();

    public Account addAccount(Account account) {
        // Add new Person
        this.account.add(account);
        // Return last added Account from List
        return this.account.get(this.account.size() - 1);
    }

    public List<Account> getAllAccount() {
        // Return the whole List
        return this.account;
    }

    public Account updateAccount(int id, Account account) {
        // Remove existing Person with matching 'id'
        this.account.remove(id);
        // Add new Account in its place
        this.account.add(id, account);
        // Return updated Account from List
        return this.account.get(id);
    }
    
   public Account removeAccount(int id) {
        // Remove Person and return it
        return this.account.remove(id);

}}
