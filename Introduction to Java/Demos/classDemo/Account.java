import java.util.Date;

public class Account {
	
	
	// Instance variables
	
	private String accountNumber;
	private String sortCode;
	private double balance;
	private String branch;
	private String accountType;
	boolean mortgage;
	private String address;
	private String name;
    private Date openedDate;
	
	
	
	    // add constructor here
	
		
	
	
	// add methods here or getters/setters
	
	
	public double getBalance() {
		return balance;
	}
	
	public void setBalance(double balance) {
		
		//only logged in Bank staff can set balance 
		
		this.balance = balance;
	}
	
	public String getBranch() {
		return branch;
	}
	
	public void setBranch(String branch) {
		this.branch = branch;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getAccountNumber() {
		return accountNumber;
	}
	
	public String getSortCode() {
		return sortCode;
	}
	

	public boolean isMortgage() {
		return mortgage;
	}
	
	public String getName() {
		
		
		
		return name;
	}
	
	
	
	
	
	
	
	

}
